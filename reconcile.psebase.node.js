var
  async   = require('async'),
  flat    = require('flat'),
  cql     = require('cairn-query-language'),
  bb      = require('functional-blackbox'),
  redis   = require('redis'),
  
  rKeys   = {
    knownSources  : 'psebase:known-sources',
    knownInstitutions
                  : 'psebase:known-institutions',
    
    constituentPrefix
                  : 'psebase:institutions-constituent:',
    
    generalStack  : 'psebase:general-stack',
    prefixSpecificStack : 'psebase:specific-stack:',
    prefixOutput : 'psebase:reconciled:'
  },
  
  unflat  = flat.unflatten,
  client  = redis.createClient(),
  
  reconcilePair,
  
  knownInstitutions,
  knownSources,
  
  setupMulti;

function getPair(inObj,cb) {
  var
    getMulti = client.multi();
    
  inObj.keys.forEach(function(aKey) {
    getMulti.hgetall(aKey);
  });
  
  getMulti.exec(bb.aug(inObj,'allAsStored',cb))
}

/*function unflatPair(inObj,cb) {
  inObj.pair = inObj.pairAsStored.map(unflat);
  
  cb(null,inObj);
}*/

function getGeneralStack(inObj,cb) {
  client.lrange(
    rKeys.generalStack,
    0,
    -1,
    bb.aug(inObj,'generalStack',cb)
  );
}

function getSpecificStack(inObj,cb) {
  client.lrange(
    rKeys.prefixSpecificStack+inObj.slug,
    0,
    -1,
    bb.aug(inObj,'specificStack',cb)
  );
}

function reconciliation(inObj,cb) {
  var
    dataObj = {};

  
  inObj.allAsStored.forEach(function(anObj,index){
    dataObj[inObj.sources[index]] = anObj;
  });

  inObj.completeStack = inObj.specificStack.concat(inObj.generalStack)

  cql.reconcile(
    inObj.completeStack,
    dataObj,
    bb.aug(inObj,'reconciled',cb)
  )
}
reconcilePair = async.seq(
  getPair,
  getGeneralStack,
  getSpecificStack,
  reconciliation
);

function writeReconciled(writeObj, cb) {
  console.log('starting',writeObj.slug);
  reconcilePair(
    writeObj,
    function(err,results){
      var
        writeMulti = client.multi();
        
      if (err) {
        throw err;
      } else {
        /*console.log('-------------------');
        console.log(results.reconciled)
        console.log('-------------------');*/
        writeMulti
          .del(rKeys.prefixOutput+results.slug)
          .hmset(
            rKeys.prefixOutput+results.slug,
            results.reconciled
          )
          .exec(function(err) {
            if (!err) {
              console.log('wrote', writeObj.slug);
            }
            cb(err);
          });

      }
    }
  );
}

setupMulti = client.multi();
setupMulti
  .smembers(rKeys.knownSources)
  .smembers(rKeys.knownInstitutions)
  .exec(function(err, knownArray) {
    var
      inObjs = [];
    
    if (err) {
      throw err;
    } else {
      knownSources = knownArray[0];
      knownInstitutions = knownArray[1];
      
      
      knownInstitutions.forEach(function(anInstitutionKey) {
        var
          anInObj = {};
          
        anInObj.slug = anInstitutionKey;
        
        anInObj.sources = [];
        anInObj.keys = [];
        knownSources.forEach(function(aSourceKey) {
          anInObj['keys'].push(
            rKeys.constituentPrefix+aSourceKey+':'+anInstitutionKey
          );
          anInObj.sources.push(aSourceKey);
        });
        inObjs.push(anInObj);
      });
      
      async.each(
        inObjs,
        writeReconciled,
        function(err) {
          if (err) {
            throw err;
          } else {
            console.log('done');
            client.quit();
          }
        }
      );
    }
  }
);